from django.db import models

class Professor(models.Model):
    
    nome = models.CharField(max_length=255, null=False, blank=False)
    email = models.CharField(max_length=255, null=False, blank=False)
    siape = models.CharField(max_length=10, null=False, blank=False)

    objetos = models.Manager()

class Aluno(models.Model):

    nome = models.CharField(max_length=255, null=False, blank=False)
    email = models.CharField(max_length=255, null=False, blank=False)
    matricula = models.CharField(max_length=10, null=False, blank=False)

    objetos = models.Manager()

class Trabalho(models.Model):
    
    titulo = models.CharField(max_length=500, null=False, blank=False)
    matricula = models.CharField(max_length=10, null=False, blank=False)
    siape = models.CharField(max_length=10, null=False, blank=False)
    autor = models.ForeignKey('Aluno', on_delete=models.CASCADE, related_name='autores')
    orientador = models.OneToOneField('Professor', on_delete=models.SET_NULL, null=True)

    objetos = models.Manager()