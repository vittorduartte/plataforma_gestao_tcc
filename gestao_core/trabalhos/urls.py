from django.shortcuts import render
from django.urls import path

from . import views

app_name = 'trabalhos'

urlpatterns = [
    path('', views.index, name='index'),
    path('professores', views.listar_professores, name='professores')
]