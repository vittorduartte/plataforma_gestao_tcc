from django.shortcuts import render
from django.views.generic import ListView
from gestao_core.models import Professor

# Create your views here.
def index(request):
    template = 'trabalhos/layouts/index.html'
    contexto = {'contexto':'<h1> meu contexto </h1>'}
    return render(request, template, contexto )

def listar_professores(request):
    professores = Professor.objetos.all()
    template = 'professores.html'
    
    return render(request, template, {'professores':professores})

